function getRandomQuote() {
  /* This function gets a random quote from the quotzzy.co api 
      and displays it on the webpage */
  $("#quote").html("");
  $("#author").html("");
  $("#loading").show();
  $.get("http://www.quotzzy.co/api/quote", function(data) {
    // api request to fetch random quote
    $("#loading").hide();
    var quote = data.text;
    var author;
    if(data.author == null) {
      author = "Unknown";
    }
    else {
      author = data.author.name;
    }
    // display quote in quote element on webpage
    $("#quote").html(`"${quote}"`);
    // display author in author element on webpage
    $("#author").html(`- ${author}`);
    // add quote to be included in tweet
    var url = "https://twitter.com/intent/tweet?hashtags=quote&";
    $("#tweet").attr("href", `${url}text="${quote}" ${author}`);
  })
  .fail(function() {
    $("#loading").hide();
    alert('Sorry, an error occured');
  });
}
window.getRandomQuote = getRandomQuote;
